import React from 'react';
import './App.sass';
import {AsideContainer} from "./components/Aside/AsideContainer";
import {SaleContainer} from "./components/Sale/SaleContainer";
import {NewContainer} from "./components/New/NewContainer";
import {NewsContainer} from "./components/News/NewsContainer";
import {changeLang, HeaderContainer} from "./components/Header/HeaderContainer";
import {FooterContainer} from "./components/Footer/FooterContainer";
import {Route} from "react-router-dom";
import ReactDOM from "react-dom";
import {MenuContainer} from "./components/Menu/MenuContainer";

function App() {
    const menuPortal = ReactDOM.createPortal(<MenuContainer changeLang={changeLang}/>, document.querySelector('#menu'))
    return (
        <div className="container">
            <HeaderContainer/>
            <div className="app">
                <AsideContainer/>
                <SaleContainer/>
                <NewContainer/>
                <NewsContainer/>
            </div>
            <FooterContainer/>
            <Route exact path='/menu' render={() => menuPortal} />
        </div>

    );
}

export default App;

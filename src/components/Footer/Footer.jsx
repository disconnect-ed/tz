import React from "react";
import './Footer.sass'
import fb from '../../assets/img/Footer/fb.svg'
import tw from '../../assets/img/Footer/tw.svg'
import you from '../../assets/img/Footer/you.svg'
import red from '../../assets/img/Footer/red.svg'
import award1 from '../../assets/img/Footer/award1.svg'
import award2 from '../../assets/img/Footer/award2.svg'
import award3 from '../../assets/img/Footer/award3.svg'
import award4 from '../../assets/img/Footer/award4.svg'

export const Footer = () => {
    return (
        <footer className="footer">
            <div className="footer-block_first">
                <h4 className="footer-rating">
                    4.9
                </h4>
                <div className="footer-info">
                    <p className='footer-info__text'>Great service and price!</p>
                    <div>
                        <span className='footer-info-rating'></span>
                        <span className='footer-info-name'>David Smith</span>
                    </div>
                </div>
            </div>
            <ul className="footer-block footer-social">
                <li className="footer-social__link">
                    <a href="#"><img src={fb} alt="facebook"/></a>
                </li>
                <li className="footer-social__link">
                    <a href="#"><img src={tw} alt="facebook"/></a>
                </li>
                <li className="footer-social__link">
                    <a href="#"><img src={you} alt="facebook"/></a>
                </li>
                <li className="footer-social__link">
                    <a href="#"><img src={red} alt="facebook"/></a>
                </li>
            </ul>
            <ul className="footer-block footer-awards">
                <li className="footer-awards__link">
                    <a href="#"><img src={award1} alt="award"/></a>
                </li>
                <li className="footer-awards__link">
                    <a href="#"><img src={award2} alt="award"/></a>
                </li>
                <li className="footer-awards__link">
                    <a href="#"><img src={award3} alt="award"/></a>
                </li>
                <li className="footer-awards__link">
                    <a href="#"><img src={award4} alt="award"/></a>
                </li>
            </ul>
        </footer>
    )
}
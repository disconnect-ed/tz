import React, {useState} from "react";
import {Header} from "./Header";
import {CartContainer} from "../Cart/CartContainer";
import i18n from "i18next";

export const changeLang = (lang) => {
    i18n.changeLanguage(lang)
}

export const HeaderContainer = () => {

    const [cartState, cartIsOpen] = useState(false)
    const CartWrapper = <CartContainer cartIsOpen={cartIsOpen}/>



    return (
        <Header CartWrapper={CartWrapper} cartState={cartState} cartIsOpen={cartIsOpen}/>
    )
}
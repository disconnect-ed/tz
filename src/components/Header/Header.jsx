import React from "react";
import logo from '../../assets/img/logo.png'
import earth from '../../assets/img/Header/earth.svg'
import user from '../../assets/img/Header/user.svg'
import balance from '../../assets/img/Header/balance.svg'
import bell from '../../assets/img/Header/bell.svg'
import cart from '../../assets/img/Header/cart.svg'
import arrow from '../../assets/img/Aside/arrow_down.svg'
import controls from '../../assets/img/Header/controls.svg'
import './Header.sass'
import {NavLink} from "react-router-dom";

export const Header = ({CartWrapper, cartState, cartIsOpen}) => {
    return (
        <header className='header'>
            <NavLink className='header-menu' to='/menu'/>
            <button className="header__btn">
                <img src={controls} alt="controls" className="header-account__icon"/>
            </button>
            <img src={logo} alt="balance" className="header__logo header__logo_main"/>
            <button className="header__btn">
                <img src={cart} alt="cart" className="header-account__icon"/>
            </button>
            <div className="header-block">
                <img src={logo} alt="REX" className="header__logo "/>
                <div className="header-lang">
                    <img src={earth} alt="language" className="header-lang__icon"/>
                    <button className="header-lang__btn">ENG</button>
                </div>
            </div>
            <div className="header-block">
                <div className="header-account__btn">
                    <button>
                        <img src={user} alt="user" className="header-account__icon"/>
                        <p>MY ACCOUNT</p>
                        <img src={arrow} alt="arrow" className="header-account__icon"/>
                    </button>
                </div>
                <div className="header-account__btn">
                    <button >
                        <img src={balance} alt="balance" className="header-account__icon"/>
                    </button>
                </div>
                <div className="header-account__btn">
                    <button >
                        <img src={bell} alt="bell" className="header-account__icon"/>
                    </button>
                </div>

                <div className="header-account-cart header-account__btn">
                    <p>3</p>
                    <button onClick={() => cartIsOpen(!cartState)}>
                        <img src={cart} alt="cart" className="header-account__icon"/>
                    </button>
                    {cartState &&
                    CartWrapper
                    }
                </div>

            </div>
        </header>
    )
}
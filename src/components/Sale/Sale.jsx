import React from "react";
import {SectionTitle} from "../common/SectionTitle/SectionTitle";
import {ProductCard} from "../common/ProductCard/ProductCard";
import './Sale.sass'

export const Sale = ({t}) => {
    return (
        <section className='sale'>
            <SectionTitle title={t('sale.title')} />
            <div className="sale-cards-wrap">
                <div className="sale-cards">
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                </div>
            </div>
        </section>
    )
}
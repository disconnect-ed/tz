import React from "react";
import {Sale} from "./Sale";
import {useTranslation} from "react-i18next";

export const SaleContainer = () => {

    const {t, i18n } = useTranslation()

    return (
        <Sale t={t}/>
    )
}
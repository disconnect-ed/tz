import React from "react";
import arrowUp from '../../../../assets/img/Aside/arrow_up.svg'
import arrowDown from '../../../../assets/img/Aside/arrow_down.svg'
import './Collapse.sass'
import {CollapseCard} from "./CollapseCard";

import btc from '../../../../assets/img/Aside/btc.svg'
import ppc from '../../../../assets/img/Aside/ppc.svg'
import etp from '../../../../assets/img/Aside/etp.svg'
import {CollapseHeader} from "./CollapseHeader";

export const CollapseWithCards = ({title, isOpen, setIsOpen}) => {
    return (
        <div className='collapse'>
            {/*<div className='collapse-header' onClick={() => setIsOpen(!isOpen)}>*/}
            {/*    <p className="collapse-header__title">*/}
            {/*        {title}*/}
            {/*    </p>*/}
            {/*    <div className="collapse-header__icon">*/}
            {/*        <img src={isOpen ? arrowUp : arrowDown} alt="arrow"/>*/}
            {/*    </div>*/}
            {/*</div>*/}
            <CollapseHeader title={title} isOpen={isOpen} setIsOpen={setIsOpen}/>
            {isOpen &&
            <div className="collapse-cards">
                <div className='collapse-cards-row'>
                    <CollapseCard itemImg={btc} itemTitle='BTC'/>
                    <CollapseCard itemImg={ppc} itemTitle='PPC'/>
                    <CollapseCard itemImg={etp} itemTitle='ETP'/>
                </div>
                <div className='collapse-cards-row'>
                    <CollapseCard itemImg={btc} itemTitle='BTC'/>
                    <CollapseCard itemImg={ppc} itemTitle='PPC'/>
                    <CollapseCard itemImg={etp} itemTitle='ETP'/>
                </div>
            </div>
            }
        </div>
    )
}
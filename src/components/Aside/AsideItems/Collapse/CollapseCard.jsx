import React from "react";

export const CollapseCard = ({itemTitle, itemImg}) => {
    return (
        <div className="collapse-card">
            <img src={itemImg} alt="Logo" className="collapse-card__img"/>
            <h5 className="collapse-card__title">
                {itemTitle}
            </h5>
        </div>
    )
}
import React from "react";

export const CollapseButton = ({buttonTitle, onClick}) => {
    return (
        <button onClick={onClick} className="collapse-button">
                {buttonTitle}
        </button>
    )
}
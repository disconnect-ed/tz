import React from "react";
import './Collapse.sass'
import {CollapseHeader} from "./CollapseHeader";
import {CollapseButton} from "./CollapseButton";

export const CollapseWithButtons = ({title, isOpen, setIsOpen}) => {
    return (
        <div className='collapse'>
            <CollapseHeader title={title} isOpen={isOpen} setIsOpen={setIsOpen}/>
            {isOpen &&
            <div className="collapse-buttons">
                    <CollapseButton buttonTitle='AMD'/>
                    <CollapseButton buttonTitle='Boundary Electric'/>
                    <CollapseButton buttonTitle='Pandaminer'/>
                    <CollapseButton buttonTitle='Nvidia'/>
            </div>
            }
        </div>
    )
}
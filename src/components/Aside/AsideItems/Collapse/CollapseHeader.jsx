import React from "react";
import arrowUp from "../../../../assets/img/Aside/arrow_up.svg";
import arrowDown from "../../../../assets/img/Aside/arrow_down.svg";

export const CollapseHeader = ({title, isOpen, setIsOpen}) => {
    const activeStyle = {
        backgroundColor: '#17182E',
        borderWidth: '3px',
        borderColor: '#2E2F42'
    }
    return (
        <div style={isOpen ? activeStyle : null} className='collapse-header' onClick={() => setIsOpen(!isOpen)}>
            <p className="collapse-header__title">
                {title}
            </p>
            <div className="collapse-header__icon">
                <img src={isOpen ? arrowUp : arrowDown} alt="arrow"/>
            </div>
        </div>
    )
}
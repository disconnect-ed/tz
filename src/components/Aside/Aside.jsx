import React from "react";
import {CollapseWithCards} from "./AsideItems/Collapse/CollapseWithCards";
import './Aside.sass'
import {CollapseWithButtons} from "./AsideItems/Collapse/CollapseWithButtons";
import {CustomInput} from "../common/CustomInput/CustomInput";
import {SectionTitle} from "../common/SectionTitle/SectionTitle";

export const Aside = ({algorithmIsOpen, setAlgorithmIsOpen, setCoinIsOpen, coinIsOpen,
                          manufacturerIsOpen, setManufacturerIsOpen, equipmentIsOpen, setEquipmentOpen,
                      t}) => {
    return (
        <aside className='aside'>
            <div className="aside-wrap">
                <SectionTitle title={t("filter.title")}/>
                <CollapseWithCards title={t("filter.algorithm")} isOpen={algorithmIsOpen}
                                   setIsOpen={setAlgorithmIsOpen}
                />
                <CollapseWithCards title={t("filter.coin")} isOpen={coinIsOpen}
                                   setIsOpen={setCoinIsOpen}
                />
                <CollapseWithButtons title={t("filter.equipment")} isOpen={manufacturerIsOpen}
                                     setIsOpen={setManufacturerIsOpen}
                />
                <CollapseWithButtons title={t("filter.manufacturer")} isOpen={equipmentIsOpen}
                                     setIsOpen={setEquipmentOpen}
                />
                <CustomInput type='text' placeholder={t("filter.minPrice")}/>
                <CustomInput type='text' placeholder={t("filter.maxPrice")}/>
                <CustomInput type='text' placeholder={t("filter.search")}/>
            </div>
        </aside>
    )
}
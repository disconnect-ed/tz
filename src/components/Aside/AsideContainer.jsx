import React, {useState} from 'react'
import {Aside} from "./Aside";
import {useTranslation} from "react-i18next";
import i18n from "i18next";

export const AsideContainer = () => {

    const [algorithmIsOpen, setAlgorithmIsOpen] = useState(false)
    const [coinIsOpen, setCoinIsOpen] = useState(false)
    const [manufacturerIsOpen, setManufacturerIsOpen] = useState(false)
    const [equipmentIsOpen, setEquipmentOpen] = useState(false)

    const {t, i18n } = useTranslation()

    return (
        <Aside algorithmIsOpen={algorithmIsOpen} setAlgorithmIsOpen={setAlgorithmIsOpen}
               coinIsOpen={coinIsOpen} setCoinIsOpen={setCoinIsOpen}
               manufacturerIsOpen={manufacturerIsOpen} setManufacturerIsOpen={setManufacturerIsOpen}
               equipmentIsOpen={equipmentIsOpen} setEquipmentOpen={setEquipmentOpen}
               t={t}
        />
    )
}
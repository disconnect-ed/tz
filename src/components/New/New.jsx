import React from "react";
import {SectionTitle} from "../common/SectionTitle/SectionTitle";
import {ProductCard} from "../common/ProductCard/ProductCard";
import './New.sass'

export const New = ({t}) => {
    return (
        <section className='new'>
            <SectionTitle title={t('new.title')}/>
            <div className="new-cards-wrap">
                <div className="new-cards">
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                    <ProductCard/>
                </div>
            </div>
        </section>
    )
}
import React from "react";
import {New} from "./New";
import {useTranslation} from "react-i18next";

export const NewContainer = () => {

    const {t, i18n } = useTranslation()

    return (
        <New t={t}/>
    )
}
import React from "react";
import './Cart.sass'

export const CartRadio = ({img, value}) => {
    return (
        <label className='cart-radio'>
            <input value={value} type="radio" className='radio' name="card"/>
                <div style={{backgroundImage: `url(${img})`}} className="fake">

                </div>
        </label>
    )
}
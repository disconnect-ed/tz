import React from "react";
import './Cart.sass'
import cartProduct from '../../assets/img/cart-product.png'


export const CartItem = () => {
    return (
        <div className="cart-item">
            <div className="cart-item__img">
                <img src={cartProduct} alt="product"/>
            </div>
            <div className="cart-item-block">
                <p className="cart-item__title">Innosilicon A6 1.23 GH/s</p>
                <div className="cart-item__info">
                    <span className="cart-item__count">999</span>
                    <p className="cart__price">$ 1000.00</p>
                </div>
            </div>
        </div>
    )
}
import React, {useState} from "react";
import {Cart} from "./Cart";

export const CartContainer = ({cartIsOpen}) => {

    const [billingDetails, showBillingDetails] = useState(false)
    const [paymentMethod, showPaymentMethod] = useState(false)
    const [isBuy, setBuy] = useState(false)

    const onPaymentSubmit = (e) => {
        e.preventDefault()
        showPaymentMethod(!paymentMethod)
    }

    const onBuySubmit = (e) => {
        e.preventDefault()
        setBuy(!isBuy)
    }

    return (
        <Cart billingDetails={billingDetails} showBillingDetails={showBillingDetails}
              paymentMethod={paymentMethod} onPaymentSubmit={onPaymentSubmit}
              onBuySubmit={onBuySubmit} isBuy={isBuy} cartIsOpen={cartIsOpen}
        />
    )
}
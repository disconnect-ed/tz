import React from "react";
import './Cart.sass'
import {CartItem} from "./CartItem";
import {MainButton} from "../common/Buttons/MainButton";
import {CustomInput} from "../common/CustomInput/CustomInput";
import {CartRadio} from "./CartRadio";
import visa from '../../assets/img/Cart/visa.png'
import paypal from '../../assets/img/Cart/paypal.png'
import coinpayments from '../../assets/img/Cart/coinpayments.png'
import skrill from '../../assets/img/Cart/skrill.png'
import webmoney from '../../assets/img/Cart/webmoney.png'
import alipay from '../../assets/img/Cart/alipay.png'
import {BlueButton} from "../common/Buttons/BlueButton";

export const Cart = ({billingDetails, showBillingDetails, onPaymentSubmit, cartIsOpen,
                         paymentMethod, onBuySubmit, isBuy}) => {

    if (isBuy) {
        return (
            <div className="cart">
                <svg width='16' height='8' className='triangle'>
                    <polyline points="0,8 8,0 16,8"/>
                </svg>
                <div className="cart-wrap">
                    <Success cartIsOpen={cartIsOpen}/>
                </div>
            </div>
        )
    }

    return (
        <div className='cart'>
            <svg width='16' height='8' className='triangle'>
                <polyline points="0,8 8,0 16,8"/>
            </svg>
            <div className="cart-wrap">
                <CartWithItems billingDetails={billingDetails} showBillingDetails={showBillingDetails}/>
                {billingDetails &&
                <BillingDetails paymentMethod={paymentMethod} onPaymentSubmit={onPaymentSubmit}/>
                }
                {paymentMethod &&
                <ShippingDetails onBuySubmit={onBuySubmit}/>
                }
            </div>
        </div>
    )
}

const CartWithItems = ({billingDetails, showBillingDetails}) => {
    return (
        <div className="cart-section">
            <div className="cart-block">
                <p className='cart-block__text'>You have 3 items in your cart</p>
                <button className='cart-block__btn'></button>
            </div>
            <div className="cart-items">
                <CartItem/>
                <CartItem/>
                <CartItem/>
            </div>
            {!billingDetails &&
            <div className="cart-block">
                <div className="cart-block__buttons">
                    <MainButton onClick={() => showBillingDetails(!billingDetails)} title='Continue'/>
                    <MainButton title='Remove All'/>
                </div>
                <p className="cart__price">11111</p>
            </div>
            }
        </div>
    )
}

const BillingDetails = ({onPaymentSubmit, paymentMethod}) => {
    return (
        <div className='cart-section'>
            <form className='cart-form cart-form_billing' onSubmit={(e) => onPaymentSubmit(e, true)}>
                <div className="cart-block">
                    <p className='cart-block__text'>Enter your Billing details</p>
                    <button className='cart-block__btn'></button>
                </div>
                <div className="cart-form-inputs">
                    <CustomInput type='text' placeholder='First name*'/>
                    <CustomInput placeholder='Last name*'/>
                    <CustomInput placeholder='Company name (optional)'/>
                    <CustomInput placeholder='Country*'/>
                    <CustomInput placeholder='House nubmer and street name*'/>
                    <CustomInput placeholder='Apartment, suite, unit etc (optional)'/>
                    <CustomInput placeholder='Town / City*'/>
                    <CustomInput placeholder='State*'/>
                    <CustomInput placeholder='ZIP*'/>
                    <CustomInput placeholder='Phone*'/>
                    <CustomInput placeholder='Email address*'/>
                    <CustomInput placeholder='Create account password*'/>
                </div>
                {!paymentMethod &&
                <div className="cart-block">
                    <div className="cart-block__buttons">
                        <MainButton type='submit' title='Continue'/>
                        <MainButton title='Remove All'/>
                    </div>
                    <p className="cart__price">11111</p>
                </div>
                }
            </form>

        </div>
    )
}

const ShippingDetails = ({onBuySubmit}) => {
    return (
        <div className='cart-section'>
            <form onSubmit={onBuySubmit} className='cart-form cart-form_shipping'>
                <div className="cart-block">
                    <p className='cart-block__text'>Choose your payment method</p>
                    <button className='cart-block__btn'></button>
                </div>
                <div className="cart-form-inputs">
                    <div className="cart-form-radio">
                        <CartRadio value={'visa'} img={visa}/>
                        <CartRadio value={'paypal'} img={paypal}/>
                        <CartRadio value={'coinpayments'} img={coinpayments}/>
                        <CartRadio value={'skrill'} img={skrill}/>
                        <CartRadio value={'webmoney'} img={webmoney}/>
                        <CartRadio value={'alipay'} img={alipay}/>
                    </div>
                    <CustomInput type='text' placeholder='XXXX XXXX XXXX XXXX   Card nubmer*'/>
                    <CustomInput placeholder='********* *********   Card holder name*'/>
                    <CustomInput placeholder='XX / XX   Expire date*'/>
                    <CustomInput placeholder='XXX   CVV*'/>
                </div>


                <div className="cart-block">
                    <div className="cart-block__buttons">
                        <BlueButton type='submit' title='Buy'/>
                        <MainButton title='Back'/>
                    </div>
                    <p className="cart__price">11111</p>
                </div>

            </form>


        </div>
    )
}

const Success = ({cartIsOpen}) => {
    return (
        <div className='cart-section'>
            <div className="cart-block">
                <p className='cart-block__text'>Successful purchase</p>
                <button onClick={() => cartIsOpen(false)} className='cart-block__btn'></button>
            </div>
            <div className="cart-message">
                <p>Thank you for using our marketplace. Your Order number is <span
                    style={{color: '#00A651'}}>#23542</span>. If you still have any questions
                    click <a href='#' style={{color: '#FCD501', textDecoration: 'underline'}}>here</a> to ask! </p>
            </div>
        </div>
    )
}
import React from "react";
import './CustomInput.sass'

export const CustomInput = ({placeholder, onChange, type}) => {
    return (
        <input className='custom-input' placeholder={placeholder} onChange={onChange} type={type}/>
    )
}
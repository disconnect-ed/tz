import React from "react";
import './SectionTitle.sass'

export const SectionTitle = ({title}) => {
    return (
        <h3 className="section__title">
            {title}
        </h3>
    )
}
import React from "react";
import './ProductCard.sass'
import star from '../../../assets/img/star.svg'
import img from '../../../assets/img/OnSale/1.png'
import {MainButton} from "../Buttons/MainButton";
import {MainLink} from "../Links/MainLink";

export const ProductCard = () => {
    return (
        <div className="product-card">
            <img className='no-image' src="http://placehold.it/1000x1000" />
            {/*<div className="product-card-header">*/}
            {/*    <p className="product-card-header__text">*/}
            {/*        USED Antminer S910.5-14.5 th/s*/}
            {/*    </p>*/}
            {/*    <div className="product-card-header__icon">*/}
            {/*        <img src={star} alt="favorite"/>*/}
            {/*    </div>*/}
            {/*</div>*/}
            {/*<div className="product-card__img">*/}
            {/*    <img src={img} alt="product"/>*/}
            {/*</div>*/}
            {/*<div className="product-card-footer">*/}
            {/*    <h4 className="product-card-footer__price">*/}
            {/*        $100 - $200*/}
            {/*    </h4>*/}
            {/*    <p className="product-card-footer__text">*/}
            {/*        PSU*/}
            {/*    </p>*/}
            {/*</div>*/}
            {/*<ProductCardActive/>*/}
        </div>
    )
}

const ProductCardActive = () => {
    return (
        <div className="product-card product-card_active">
            <div className="product-card-header_active">
                <p className="product-card-header__text">
                    USED Antminer S910.5-14.5 th/s
                </p>
                <div className="product-card-header__icon">
                    <img src={star} alt="favorite"/>
                </div>
            </div>
            <div className="product-card-footer_active">
                <h4 className="product-card-footer__price">
                    $100 - $200
                </h4>
                <div className="product-card-footer__buttons">
                    <MainLink title='Details'/>
                    <MainButton title='Add to cart'/>
                </div>
            </div>
        </div>
    )
}
import React from "react";
import './Links.sass'

export const MainLink = ({title}) => {
    return (
        <a className='main-link' href="#">
            {title}
        </a>
    )
}
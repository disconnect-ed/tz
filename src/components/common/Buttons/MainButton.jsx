import React from "react";
import './Buttons.sass'

export const MainButton = ({onClick, title, type}) => {
    return <button type={type} className='main-button' onClick={onClick}>
        {title}
    </button>
}
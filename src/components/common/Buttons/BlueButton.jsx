import React from "react";
import './Buttons.sass'

export const BlueButton = ({onClick, title, type}) => {
    return <button type={type} className='blue-button' onClick={onClick}>
        {title}
    </button>
}
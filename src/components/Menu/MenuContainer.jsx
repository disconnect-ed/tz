import React, {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {Menu} from "./Menu";

export const MenuContainer = ({changeLang}) => {

    useEffect(() => {
        document.querySelector('body').classList.add('lock')
        // menuIsOpen(true)
    }, [])

    const onClick = () => {
        document.querySelector('body').classList.remove('lock')
    }

    const {t, i18n} = useTranslation()

    return (
        <Menu changeLang={changeLang} t={t} onClick={onClick}/>
    )
}
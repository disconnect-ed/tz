import React, {useEffect, useState} from "react";
import './Menu.sass'
import {NavLink} from "react-router-dom";
import {useTranslation} from "react-i18next";

export const Menu = ({changeLang, onClick, t}) => {





    return (
        <section className="menu">
            <div className="menu-lang">
                <div className="menu-lang__btns">
                    <button onClick={() => changeLang('en')} className="menu-lang__btn">EN</button>
                    <button onClick={() => changeLang('ru')} className="menu-lang__btn">RU</button>
                    <button onClick={() => changeLang('by')} className="menu-lang__btn">BY</button>
                    <button onClick={() => changeLang('ua')} className="menu-lang__btn">UA</button>
                    <button onClick={() => changeLang('pl')} className="menu-lang__btn">PL</button>
                </div>
            </div>
            <NavLink onClick={onClick} className='menu-out' to='/'/>
            <div className="menu__link-wrap">
                <NavLink onClick={onClick} className='menu__link' to='/home'>
                    {t('menu.home')}
                </NavLink>
            </div>
            <div className="menu__link-wrap">
                <NavLink onClick={onClick} className='menu__link' to='/home'>
                    {t('menu.sell')}
                </NavLink>
            </div>
            <div className="menu__link-wrap">
                <NavLink onClick={onClick} className='menu__link' to='/home'>
                    {t('menu.host')}
                </NavLink>
            </div>
            <div className="menu__link-wrap">
                <NavLink onClick={onClick} className='menu__link' to='/home'>
                    {t('menu.about')}
                </NavLink>
            </div>
            <div className="menu__link-wrap">
                <NavLink onClick={onClick} className='menu__link' to='/home'>
                    {t('menu.support')}
                </NavLink>
            </div>
        </section>
    )
}
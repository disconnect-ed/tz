import React from "react";
import {News} from "./News";
import {useTranslation} from "react-i18next";

export const NewsContainer = () => {

    const {t, i18n } = useTranslation()

    return (
        <News t={t} />
    )
}
import React from "react";
import banner from '../../assets/img/banner.png'

export const NewsItem = () => {
    return (
        <div className="news-item">
            <img src={banner} alt="banner"/>
        </div>
    )
}
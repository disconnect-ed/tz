import React from 'react'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import {NewsItem} from "./NewsItem";

export const NewsSlider = () => {
    const settings = {
        dots: true,
        infinite: true,
        fade: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        customPaging: i => <div key={i} className='slider-dot'></div>
    };
    return (
        <div className="news-slider">
            <Slider {...settings}>
                <NewsItem/>
                <NewsItem/>
                <NewsItem/>
            </Slider>
        </div>
    )
}
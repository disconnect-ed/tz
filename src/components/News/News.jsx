import React from "react";
import {SectionTitle} from "../common/SectionTitle/SectionTitle";
import {NewsSlider} from "./NewsSlider";
import './News.sass'

export const News = ({t}) => {
    return (
        <section className='news'>
            <SectionTitle title={t('news.title')}/>
                <NewsSlider/>
        </section>
    )
}